#!/bin/bash

## if you need to recreate test database, use:
[[ -z "${CREATE_DB}" ]] && db_option='--reuse-db' || db_option='--create-db'


docker-compose run \
  -e DJANGO_SETTINGS_MODULE=config.settings \
  -e PYTHONUNBUFFERED=0 \
  --rm app pytest ${db_option} -s ${@:1}

docker-compose stop