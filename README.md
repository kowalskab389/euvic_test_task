# Euvic_test_task

Simple application for a test task - Bank account simulation

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kowalskab389/euvic_test_task.git
git branch -M main
git push -uf origin main
```



## Name
Bank account simulator

## Description


## Prerequisites
* Docker
* docker-compose
* python 3.10

## Installation
After cloning the repository, create .env file
```
cp .env.example .env
```
Run docker compose
```
docker-compose build
docker-compose up
```
It will create docker image and run project on http://0.0.0.0:8000/

Run init migrations with:
```
docker-compose run app python manage.py migrate
```
After first migration create superuser for administration panel using following command in terminal
```
docker exec -it euvic_test_task_container python manage.py createsuperuser
```
Enter your username, email as you desire, or press enter to apply default settings. 
It will be starting point for your app. Now you can add bankaccouns to this user and use every other endpoint as you wish.

To stop app run:
```
docker-compose down
```

## Usage
By default the service uses port 8000. http://127.0.0.1:8000

## Service urls
You can use this api:
* django admin http://127.0.0.1:8000/admin/
* via swagger (http://127.0.0.1:8000/api/v1/swagger/schema/)

## Test
To run tests run command in terminal
```
./run_tests.sh
```

