from django.contrib import admin
from django.urls import path, include

from drf_yasg import openapi
from drf_yasg.views import get_schema_view as swaggerget_schema_view

schema_view = swaggerget_schema_view(
    openapi.Info(
        title="BarbBank Swagger",
        default_version="1.0.0",
        description="API documentation of barb_bank_app",
    ),
    public=True,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("accounts.urls")),
    path(
        "api/v1/",
        include(
            [
                path("", include("api.urls")),
                path(
                    "swagger/schema/",
                    schema_view.with_ui("swagger", cache_timeout=0),
                    name="swagger-schema",
                ),
            ]
        ),
    ),
]
