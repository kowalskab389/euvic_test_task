from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("signupaccount/", views.signupaccount, name="signupaccount"),
    path("logout/", views.logoutaccount, name="logoutaccount"),
    path("login/", views.loginaccount, name="loginaccount"),
]
