FROM python:3.10
MAINTAINER Barbara Kowalska

ENV PYTHONUNBUFFERED=1

WORKDIR /euvic_test_task

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip && \
    pip3 install -r requirements.txt

COPY . .

RUN chmod +x run_tests.sh
