from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from .views import (
    all_bank_accounts_list,
    user_bank_accounts_list,
    create_bank_account,
    bank_account_timeline,
    bank_account_details,
    money_transfer_list,
    create_money_transfer,
    money_deposit,
    money_withdraw,
)
from .viewsets import UserViewSet, BankAccountViewSet, MoneyTransferViewSet


router = routers.DefaultRouter()

router.register(r"users", UserViewSet)
router.register(r"bankaccounts", BankAccountViewSet)
router.register(r"moneytransfers", MoneyTransferViewSet)

# schema_view.cls.schema = None

urlpatterns = [
    path("", include(router.urls)),
    path("auth/", obtain_auth_token),
    path("bank_accounts/", all_bank_accounts_list, name="all_bank_accounts_list"),
    path(
        "bank_accounts/<int:owner_id>/list/",
        user_bank_accounts_list,
        name="user_bank_accounts_list",
    ),
    path(
        "bank_accounts/<int:owner_id>/create/",
        create_bank_account,
        name="create_bank_account",
    ),
    path(
        "bank_accounts/<int:full_account_number>/timeline/",
        bank_account_timeline,
        name="bank_account_timeline",
    ),
    path(
        "bank_accounts/<int:full_account_number>/details/",
        bank_account_details,
        name="bank_account_details",
    ),
    path(
        "money_transfers/<int:full_account_number>/list/",
        money_transfer_list,
        name="money_transfer_list",
    ),
    path(
        "money_transfers/create/", create_money_transfer, name="create_money_transfer"
    ),
    path("bank_accounts/deposit", money_deposit, name="money_deposit"),
    path("bank_accounts/withdraw", money_withdraw, name="money_withdraw"),
]
