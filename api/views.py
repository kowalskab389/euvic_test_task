from rest_framework import status
from rest_framework.schemas.openapi import AutoSchema
from rest_framework.generics import (
    CreateAPIView,
    GenericAPIView,
    RetrieveAPIView,
    ListAPIView,
)
from rest_framework.response import Response
from django.contrib.auth.models import User
from api.models import BankAccount, MoneyTransfer
from api.logic import make_transfer, transfers_to_timeline, make_deposit, make_withdraw
from api.serializers import (
    BankAccountSerializer,
    BankAccountCreateSerializer,
    DepositSerializer,
    MoneyTransferSerializer,
    MoneyTransferCreateSerializer,
    WithdrawSerializer,
)


class AllBankAccountsList(ListAPIView):
    """
    Returns a list of all bank accounts in db

    """

    serializer_class = BankAccountSerializer
    queryset = BankAccount.active.all()


class UserBankAccountsList(ListAPIView):
    """
    Returns a list of bank accounts associated with the given user
    """

    serializer_class = BankAccountSerializer
    queryset = BankAccount.active.all()

    def get(self, request, *args, **kwargs):
        owner_id = kwargs.get("owner_id")
        queryset = BankAccount.active.get_related_to_user(owner_id)
        serializer = BankAccountSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CreateBankAccount(CreateAPIView):
    """
    Creates a new bank account for a specific user
    """

    serializer_class = BankAccountCreateSerializer
    schema = AutoSchema()

    def perform_create(self, serializer):
        owner = self.kwargs.get("owner_id")
        owner = User.objects.filter(id=owner).get()
        return serializer.save(owner=owner)


class BankAccountDetails(RetrieveAPIView):
    """
    Returns bank account information based on the full account number.

    """

    serializer_class = BankAccountSerializer
    queryset = BankAccount.active.all()
    lookup_field = "full_account_number"


class BankAccountTimeline(GenericAPIView):
    """
    List of Incomes and Expenses for single bank account.

    """

    serializer_class = MoneyTransferSerializer
    queryset = MoneyTransfer.transactions_registered.all()

    def get(self, request, *args, **kwargs):
        full_number = kwargs.get("full_account_number")
        queryset = MoneyTransfer.transactions_registered.get_all_by_full_number(
            full_number
        )
        serializer = MoneyTransferSerializer(queryset, many=True)
        time_line = transfers_to_timeline(serializer.data, full_number)

        return Response(time_line)


class MoneyTransferList(GenericAPIView):
    """
    Returns a list of money transfers associated with the given bank_account (in source/destination)

    """

    serializer_class = MoneyTransferCreateSerializer
    queryset = MoneyTransfer.transactions_registered.all()

    def get(self, request, *args, **kwargs):
        full_number = kwargs.get("full_account_number")
        queryset = MoneyTransfer.transactions_registered.get_all_by_full_number(
            full_number
        )
        serializer = MoneyTransferSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CreateMoneyTransfer(CreateAPIView, GenericAPIView):
    """
    Creates new money transfer and transfers funds between beneficiary and destination accounts.

    """

    serializer_class = MoneyTransferCreateSerializer
    schema = AutoSchema()

    def perform_create(self, serializer):
        if serializer.is_valid():
            source = serializer.validated_data["source"].get("full_account_number")
            source = BankAccount.active.get_by_full_number(source)
            destination = serializer.validated_data["destination"].get(
                "full_account_number"
            )
            destination = BankAccount.active.get_by_full_number(destination)
            transfer = serializer.save(source=source, destination=destination)
            make_transfer(source, destination, transfer)
            return Response({"message": "Success"})
        else:
            return Response({"message": "failed", "details": serializer.errors})


class MoneyDeposit(CreateAPIView, GenericAPIView):
    """
    Adding founds to existing bank account - cash deposit Machine

    """

    queryset = BankAccount.active.all()
    serializer_class = DepositSerializer

    def perform_create(self, serializer):
        if serializer.is_valid():
            destination = serializer.validated_data["full_account_number"]
            destination = BankAccount.active.get_by_full_number(destination)
            make_deposit(destination, serializer.validated_data["money_amount"])
            return Response({"message": "Balance updated successfully"})
        else:
            return Response({"message": "failed", "details": serializer.errors})


class MoneyWithdraw(CreateAPIView, GenericAPIView):
    """
    Withdraw money from existing bank account # cash machine

    """

    queryset = BankAccount.active.all()
    serializer_class = WithdrawSerializer

    def perform_create(self, serializer):
        if serializer.is_valid():
            source = serializer.validated_data["full_account_number"]
            source = BankAccount.active.get_by_full_number(source)
            make_withdraw(source, serializer.validated_data["money_amount"])
            return Response({"message": "Balance updated successfully"})
        else:
            return Response({"message": "failed", "details": serializer.errors})


all_bank_accounts_list = AllBankAccountsList.as_view()  # ok
user_bank_accounts_list = UserBankAccountsList.as_view()  # ok
create_bank_account = CreateBankAccount.as_view()  # ok
bank_account_timeline = BankAccountTimeline.as_view()  # ok
bank_account_details = BankAccountDetails.as_view()  # ok
money_transfer_list = MoneyTransferList.as_view()  # ok
create_money_transfer = CreateMoneyTransfer.as_view()  # ok
money_deposit = MoneyDeposit.as_view()
money_withdraw = MoneyWithdraw.as_view()
