from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from api.managers import BankAccountManager, MoneyTransferManager

# User = settings.AUTH_USER_MODEL


# Create your models here.
class BankAccount(models.Model):
    # class Type(models.TextChoices):
    #     CHECKING = 'PS', 'Personal Account',
    #     SAVINGS = 'SV', 'Savings Account'

    owner = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL)
    full_account_number = models.IntegerField(unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    available_founds = models.FloatField(default=0.0)
    created = models.DateTimeField(auto_now_add=True)
    active = BankAccountManager()

    class Meta:
        ordering = ["created"]
        indexes = [
            models.Index(fields=["created"]),
        ]

    def __str__(self):
        return f"{self.full_account_number}"

    @property
    def get_absolute_url(self):
        return reverse("bank_account_details", kwargs={"account_id": self.id})

    @property
    def get_new_transfer_url(self):
        return reverse(
            "create_transfer", kwargs={"account_number": self.full_account_number}
        )


class MoneyTransfer(models.Model):
    source = models.ForeignKey(
        BankAccount,
        on_delete=models.CASCADE,
        related_name="outcome_transfers",
        null=True,
    )
    destination = models.ForeignKey(
        BankAccount,
        on_delete=models.CASCADE,
        related_name="income_transfers",
        null=True,
    )
    transaction_date = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=255)
    transaction_amount = models.FloatField()
    transactions_registered = MoneyTransferManager()

    def __str__(self):
        return (
            f"From: {self.source.owner} {self.source.full_account_number} \nto: {self.destination.owner} "
            f"{self.destination.full_account_number}"
        )

    class Meta:
        ordering = ["-transaction_date"]
        indexes = [
            models.Index(fields=["transaction_date"]),
        ]
