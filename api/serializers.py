from rest_framework import serializers

from django.contrib.auth.models import User
from api.models import BankAccount, MoneyTransfer


class UserPublicSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(read_only=True)


class BankAccountTransferSerializer(serializers.Serializer):
    full_account_number = serializers.IntegerField(read_only=True)


class CurrentUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["url", "id", "username"]


class BankAccountSerializer(serializers.ModelSerializer):
    # owner = serializers.PrimaryKeyRelatedField(read_only=True)
    owner = UserPublicSerializer(read_only=True)

    # absolute_url = serializers.SerializerMethodField(read_only=True)
    # new_transfer_url = serializers.SerializerMethodField(read_only=True)
    # url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = BankAccount
        fields = [
            "id",
            "owner",
            "first_name",
            "last_name",
            "full_account_number",
            "available_founds",
            "created",
        ]


class BankAccountCreateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BankAccount
        fields = [
            "full_account_number",
            "available_founds",
        ]

    def get_owner(self, obj):
        return obj.owner.id


class MoneyTransferSerializer(serializers.HyperlinkedModelSerializer):
    source = serializers.SerializerMethodField(read_only=True)
    destination = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = MoneyTransfer
        fields = [
            "id",
            "source",
            "destination",
            "transaction_date",
            "description",
            "transaction_amount",
        ]

    def get_source(self, obj):
        if obj.source is not None:
            return obj.source.full_account_number
        else:
            return None

    def get_destination(self, obj):
        if obj.destination is not None:
            return obj.destination.full_account_number
        else:
            return None


class MoneyTransferCreateSerializer(serializers.HyperlinkedModelSerializer):
    source = serializers.IntegerField(source="source.full_account_number")
    destination = serializers.IntegerField(source="destination.full_account_number")

    class Meta:
        model = MoneyTransfer
        fields = ["source", "destination", "description", "transaction_amount"]

    def validate_source(self, value):
        try:
            BankAccount.active.get_by_full_number(value)
        except Exception:
            raise serializers.ValidationError("Source bank account does not exist")
        else:
            return value

    def validate_destination(self, value):
        try:
            BankAccount.active.get_by_full_number(value)
        except Exception:
            raise serializers.ValidationError("Destination bank account does not exist")
        else:
            return value

    def validate_transaction_amount(self, value):
        if value < 0:
            raise serializers.ValidationError(
                "You cannot transfer a negative amount of money"
            )
        elif value == 0:
            raise serializers.ValidationError(
                "Transfer zero amount of money makes no sense"
            )
        else:
            source_account_number = self.context.get("request").data.get("source")
            try:
                source_account = BankAccount.active.get_by_full_number(
                    source_account_number
                )
            except Exception:
                raise serializers.ValidationError(
                    "Can not check current balance for non existing bank account"
                )
            else:
                current_balance = source_account.available_founds
                if current_balance < value:
                    raise serializers.ValidationError("Lack of founds")
                if value < 0:
                    raise serializers.ValidationError(
                        "You cannot make a transfer with a negative amount of money"
                    )

        return value

    def create(self, validated_data):
        return MoneyTransfer(**validated_data)


class DepositSerializer(serializers.ModelSerializer):
    full_account_number = serializers.IntegerField()
    money_amount = serializers.FloatField()

    class Meta:
        model = BankAccount
        fields = ["full_account_number", "money_amount"]

    def validate_full_account_number(self, value):
        try:
            BankAccount.active.get_by_full_number(value)
        except Exception:
            raise serializers.ValidationError("Destination bank account does not exist")
        else:
            return value

    def validate_money_amount(self, value):
        if value < 0:
            raise serializers.ValidationError(
                "You cannot deposit a negative amount of money"
            )
        elif value == 0:
            raise serializers.ValidationError(
                "Deposit zero amount of money makes no sense"
            )
        else:
            return value

    def create(self, validated_data):
        return MoneyTransfer(**validated_data)


class WithdrawSerializer(serializers.ModelSerializer):
    full_account_number = serializers.IntegerField()
    money_amount = serializers.FloatField()

    class Meta:
        model = BankAccount
        fields = ["full_account_number", "money_amount"]

    def validate_money_amount(self, value):
        if value < 0:
            raise serializers.ValidationError(
                "You cannot withdraw a negative amount of money"
            )
        elif value == 0:
            raise serializers.ValidationError(
                "Withdrawing zero amount of money makes no sense"
            )
        else:
            source_account_number = self.context.get("request").data.get(
                "full_account_number"
            )
            try:
                source_account = BankAccount.active.get_by_full_number(
                    source_account_number
                )
            except Exception:
                raise serializers.ValidationError(
                    "Can not check current balance for non existing bank account"
                )
            else:
                current_balance = source_account.available_founds
                if current_balance < value:
                    raise serializers.ValidationError("Lack of founds")
                else:
                    return value

    def validate_full_account_number(self, value):
        try:
            BankAccount.active.get_by_full_number(value)
        except Exception:
            raise serializers.ValidationError("Source bank account does not exist")
        else:
            return value

    def create(self, validated_data):
        return MoneyTransfer(**validated_data)
