from django.db import transaction
from api.models import MoneyTransfer, BankAccount


@transaction.atomic
def make_transfer(source: BankAccount, dest: BankAccount, transfer: MoneyTransfer):
    source.available_founds -= transfer.transaction_amount
    dest.available_founds += transfer.transaction_amount
    source.save()
    dest.save()


def transfers_to_timeline(transfers: list, full_account_number: int) -> list:
    """
    Gets full list of money transfers related to bank account

    Returns a list of money transfers with type (income/expense/deposit/withdraw)
    and transaction amount changed according to type (negative value for expenses)
    """
    timeline = []
    for transfer in transfers:
        if transfer["source"] == full_account_number or transfer["destination"] is None:
            transfer["transaction_amount"] = -abs(transfer["transaction_amount"])
            transfer["type"] = (
                "Withdraw" if transfer["destination"] is None else "Expense"
            )

            transfer["transaction_amount"] = -abs(transfer["transaction_amount"])
        else:
            transfer["type"] = "Deposit" if transfer["source"] is None else "Income"
        timeline.append(transfer)
    return timeline


@transaction.atomic
def make_deposit(bank_account: BankAccount, money_amount: float):
    transfer = MoneyTransfer(
        source=None,
        destination=bank_account,
        description="Deposit",
        transaction_amount=money_amount,
    )
    transfer.save()
    bank_account.available_founds += money_amount
    bank_account.save()


@transaction.atomic
def make_withdraw(bank_account: BankAccount, money_amount: float):
    if money_amount <= bank_account.available_founds:
        transfer = MoneyTransfer(
            source=bank_account,
            destination=None,
            description="Withdraw",
            transaction_amount=money_amount,
        )
        transfer.save()
        bank_account.available_founds -= money_amount
        bank_account.save()
