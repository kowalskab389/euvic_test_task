import pytest
from rest_framework.test import APIClient
from django.contrib.auth.models import User

from api.models import BankAccount, MoneyTransfer


@pytest.fixture
def api_client():
    return APIClient


@pytest.fixture
def sample_user(django_user_model):
    user = django_user_model(username="user1", password="samplepwd")
    user.save()
    yield user
    user.delete()


@pytest.fixture
def api_client_with_authenticated_user(api_client, sample_user):
    client = api_client()
    client.login(username=sample_user.username, password="samplepwd")
    yield client
    client.logout()


@pytest.fixture()
def client_new_bank_account_factory(db):
    def create_client_bank_account(
        full_account_number: int,
        owner: User,
        first_name: str = "",
        last_name: str = "",
        available_founds: float = 0,
    ):
        client_bank_account = BankAccount(
            owner=owner,
            full_account_number=full_account_number,
            first_name=first_name,
            last_name=first_name,
            available_founds=available_founds,
        )
        client_bank_account.save()
        return client_bank_account

    return create_client_bank_account


@pytest.fixture
def client_new_bank_account(db, sample_user, client_new_bank_account_factory):
    account = client_new_bank_account_factory(1234, sample_user, available_founds=1000)
    yield account
    account.delete()


@pytest.fixture
def client_new_bank_account_no_founds(db, sample_user, client_new_bank_account_factory):
    account = client_new_bank_account_factory(4321, sample_user, available_founds=0.0)
    yield account
    account.delete()


@pytest.fixture
def new_transfer_factory(db):
    def create_new_transfer(
        source: BankAccount,
        destination: BankAccount,
        description: str = "",
        transaction_amount: float = 0,
    ):
        money_transfer = MoneyTransfer(
            source=source,
            destination=destination,
            description=description,
            transaction_amount=transaction_amount,
        )
        money_transfer.save()
        return money_transfer

    return create_new_transfer


@pytest.fixture
def transfer_1(
    db, client_new_bank_account, client_new_bank_account_no_founds, new_transfer_factory
):
    money_transfer = new_transfer_factory(
        client_new_bank_account,
        client_new_bank_account_no_founds,
        "money transfer",
        500.0,
    )
    yield money_transfer
    money_transfer.delete()


@pytest.fixture
def transfer_2(
    db, client_new_bank_account, client_new_bank_account_no_founds, new_transfer_factory
):
    money_transfer = new_transfer_factory(
        client_new_bank_account_no_founds,
        client_new_bank_account,
        "money transfer",
        100.0,
    )
    yield money_transfer
    money_transfer.delete()


@pytest.fixture
def transfer_3(
    db, client_new_bank_account, client_new_bank_account_no_founds, new_transfer_factory
):
    money_transfer = new_transfer_factory(
        None, client_new_bank_account, "money transfer", 100.0
    )
    yield money_transfer
    money_transfer.delete()


@pytest.fixture
def transfer_4(
    db, client_new_bank_account, client_new_bank_account_no_founds, new_transfer_factory
):
    money_transfer = new_transfer_factory(
        client_new_bank_account, None, "money transfer", 100.0
    )
    yield money_transfer
    money_transfer.delete()


@pytest.fixture
def transfer_different_account(
    db, client_new_bank_account, client_new_bank_account_no_founds, new_transfer_factory
):
    money_transfer = new_transfer_factory(
        None, client_new_bank_account_no_founds, "money transfer", 100.0
    )
    yield money_transfer
    money_transfer.delete()
