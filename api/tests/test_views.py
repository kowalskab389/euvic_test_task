import pytest
from rest_framework.reverse import reverse


@pytest.mark.django_db
def test_create_bank_account(api_client_with_authenticated_user, sample_user):
    url = reverse("create_bank_account", kwargs={"owner_id": sample_user.id})
    data = {"full_account_number": 111, "available_founds": 0}
    format = "json"
    response = api_client_with_authenticated_user.post(url, data, format)
    assert response.status_code == 201


@pytest.mark.django_db
def test_new_bank_account(client_new_bank_account):
    assert client_new_bank_account.full_account_number == 1234


@pytest.mark.django_db
def test_get_all_bank_accounts(
    api_client_with_authenticated_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
):
    url = reverse("all_bank_accounts_list")
    response = api_client_with_authenticated_user.get(url)
    response_data = response.json()

    assert response.status_code == 200
    assert response_data[0]["full_account_number"] == 1234


@pytest.mark.django_db
def test_transfer_success(
    api_client_with_authenticated_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
):
    url = reverse("create_money_transfer")
    data = {
        "source": client_new_bank_account.full_account_number,
        "destination": client_new_bank_account_no_founds.full_account_number,
        "transaction_amount": 50.0,
        "description": "Sample transfer",
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    client_new_bank_account.refresh_from_db()
    client_new_bank_account_no_founds.refresh_from_db()

    assert response.status_code == 201
    assert client_new_bank_account.available_founds == 950.0
    assert client_new_bank_account_no_founds.available_founds == 50.0


@pytest.mark.django_db
def test_transfer_fail_non_existing_source(
    api_client_with_authenticated_user, client_new_bank_account
):
    url = reverse("create_money_transfer")
    data = {
        "source": 0,
        "destination": client_new_bank_account.full_account_number,
        "transaction_amount": 150.0,
        "description": "Sample transfer",
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()

    client_new_bank_account.refresh_from_db()

    assert response.status_code == 400
    assert response_data["source"] == ["Source bank account does not exist"]
    assert response_data["transaction_amount"] == [
        "Can not check current balance for non existing bank account"
    ]
    assert client_new_bank_account.available_founds == 1000


@pytest.mark.django_db
def test_transfer_fail_non_existing_destination(
    api_client_with_authenticated_user, client_new_bank_account
):
    url = reverse("create_money_transfer")
    data = {
        "source": client_new_bank_account.full_account_number,
        "destination": 0,
        "transaction_amount": 150.0,
        "description": "Sample transfer",
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()

    client_new_bank_account.refresh_from_db()

    assert response.status_code == 400
    assert response_data["destination"] == ["Destination bank account does not exist"]
    assert client_new_bank_account.available_founds == 1000


@pytest.mark.django_db
def test_transfer_fail_lack_of_founds(
    api_client_with_authenticated_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
):
    url = reverse("create_money_transfer")
    data = {
        "source": client_new_bank_account_no_founds.full_account_number,
        "destination": client_new_bank_account.full_account_number,
        "transaction_amount": 50.0,
        "description": "Sample transfer",
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()

    client_new_bank_account.refresh_from_db()
    client_new_bank_account_no_founds.refresh_from_db()

    assert response.status_code == 400
    assert response_data["transaction_amount"] == ["Lack of founds"]
    assert client_new_bank_account.available_founds == 1000
    assert client_new_bank_account_no_founds.available_founds == 0.0


@pytest.mark.django_db
def test_transfer_fail_wrong_ammount_of_money(
    api_client_with_authenticated_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
):
    url = reverse("create_money_transfer")
    data = {
        "source": client_new_bank_account_no_founds.full_account_number,
        "destination": client_new_bank_account.full_account_number,
        "transaction_amount": 0.0,
        "description": "Sample transfer",
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account.refresh_from_db()
    client_new_bank_account_no_founds.refresh_from_db()

    assert response.status_code == 400
    assert response_data["transaction_amount"] == [
        "Transfer zero amount of money makes no sense"
    ]
    assert client_new_bank_account.available_founds == 1000
    assert client_new_bank_account_no_founds.available_founds == 0.0

    data = {
        "source": client_new_bank_account_no_founds.full_account_number,
        "destination": client_new_bank_account.full_account_number,
        "transaction_amount": -10.0,
        "description": "Sample transfer",
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account.refresh_from_db()
    client_new_bank_account_no_founds.refresh_from_db()

    assert response.status_code == 400
    assert response_data["transaction_amount"] == [
        "You cannot transfer a negative amount of money"
    ]
    assert client_new_bank_account.available_founds == 1000
    assert client_new_bank_account_no_founds.available_founds == 0.0


@pytest.mark.django_db
def test_deposit_success(
    api_client_with_authenticated_user,
    client_new_bank_account_no_founds,
):
    url = reverse("money_deposit")
    data = {
        "full_account_number": client_new_bank_account_no_founds.full_account_number,
        "money_amount": 50.0,
    }

    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account_no_founds.refresh_from_db()

    assert response.status_code == 201
    assert (
        client_new_bank_account_no_founds.available_founds
        == response_data["money_amount"]
    )


@pytest.mark.django_db
def test_deposit_fail(
    api_client_with_authenticated_user,
    client_new_bank_account_no_founds,
):
    url = reverse("money_deposit")
    data = {
        "full_account_number": client_new_bank_account_no_founds.full_account_number,
        "money_amount": 0.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    assert response.status_code == 400
    assert response_data["money_amount"] == [
        "Deposit zero amount of money makes no sense"
    ]
    assert client_new_bank_account_no_founds.available_founds == 0.0

    data = {
        "full_account_number": client_new_bank_account_no_founds.full_account_number,
        "money_amount": -20.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()

    client_new_bank_account_no_founds.refresh_from_db()

    assert response.status_code == 400
    assert response_data["money_amount"] == [
        "You cannot deposit a negative amount of money"
    ]
    assert client_new_bank_account_no_founds.available_founds == 0.0


@pytest.mark.django_db
def test_withdraw_success(
    api_client_with_authenticated_user,
    client_new_bank_account,
):
    url = reverse("money_withdraw")
    data = {
        "full_account_number": client_new_bank_account.full_account_number,
        "money_amount": 50.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")

    client_new_bank_account.refresh_from_db()

    assert response.status_code == 201
    # assert response_data["money_amount"] == ["Deposit zero amount of money makes no sense"]
    assert client_new_bank_account.available_founds == 950.0


def test_withdraw_fail(
    api_client_with_authenticated_user,
    client_new_bank_account,
):
    url = reverse("money_withdraw")
    data = {
        "full_account_number": 0,
        "money_amount": 50.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account.refresh_from_db()

    assert response.status_code == 400
    assert response_data["full_account_number"] == [
        "Source bank account does not exist"
    ]
    assert response_data["money_amount"] == [
        "Can not check current balance for non existing bank account"
    ]
    assert client_new_bank_account.available_founds == 1000

    data = {
        "full_account_number": client_new_bank_account.full_account_number,
        "money_amount": 5000.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account.refresh_from_db()

    assert response.status_code == 400
    assert response_data["money_amount"] == ["Lack of founds"]
    assert client_new_bank_account.available_founds == 1000

    data = {
        "full_account_number": client_new_bank_account.full_account_number,
        "money_amount": 0.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account.refresh_from_db()

    assert response.status_code == 400
    assert response_data["money_amount"] == [
        "Withdrawing zero amount of money makes no sense"
    ]
    assert client_new_bank_account.available_founds == 1000

    data = {
        "full_account_number": client_new_bank_account.full_account_number,
        "money_amount": -50.0,
    }
    response = api_client_with_authenticated_user.post(url, data, format="json")
    response_data = response.json()
    client_new_bank_account.refresh_from_db()

    assert response.status_code == 400
    assert response_data["money_amount"] == [
        "You cannot withdraw a negative amount of money"
    ]
    assert client_new_bank_account.available_founds == 1000


@pytest.mark.django_db
def test_get_bank_accounts(
    api_client_with_authenticated_user,
    sample_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
):

    url = reverse("user_bank_accounts_list", kwargs={"owner_id": sample_user.id})

    response = api_client_with_authenticated_user.get(url)
    response_data = response.json()

    assert response.status_code == 200
    for account in response_data:
        assert account["owner"]["id"] == sample_user.id
        assert account["owner"]["username"] == sample_user.username


@pytest.mark.django_db
def test_get_money_transfer_list(
    api_client_with_authenticated_user,
    sample_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
    transfer_1,
    transfer_2,
    transfer_3,
    transfer_4,
    transfer_different_account,
):
    url = reverse(
        "money_transfer_list",
        kwargs={"full_account_number": client_new_bank_account.full_account_number},
    )
    response = api_client_with_authenticated_user.get(url)
    response_data = response.json()

    assert response.status_code == 200
    for transfer in response_data:
        check = (
            True
            if client_new_bank_account.full_account_number
            in [transfer["source"], transfer["destination"]]
            else False
        )
        assert check is True


@pytest.mark.django_db
def test_get_timeline(
    api_client_with_authenticated_user,
    sample_user,
    client_new_bank_account,
    client_new_bank_account_no_founds,
    transfer_1,
    transfer_2,
    transfer_3,
    transfer_4,
    transfer_different_account,
):
    url = reverse(
        "bank_account_timeline",
        kwargs={"full_account_number": client_new_bank_account.full_account_number},
    )
    response = api_client_with_authenticated_user.get(url)
    response_data = response.json()

    assert response.status_code == 200
    for transfer in response_data:
        check = (
            True
            if client_new_bank_account.full_account_number
            in [transfer["source"], transfer["destination"]]
            else False
        )
        assert check is True
    assert response_data[0]["type"] == "Withdraw"
    assert response_data[0]["transaction_amount"] < 0
    assert response_data[1]["type"] == "Deposit"
    assert response_data[1]["transaction_amount"] > 0
    assert response_data[2]["type"] == "Income"
    assert response_data[2]["transaction_amount"] > 0
    assert response_data[3]["type"] == "Expense"
    assert response_data[3]["transaction_amount"] < 0
