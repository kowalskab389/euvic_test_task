from django.db import models
from django.db.models import Q


class BankAccountQuerySet(models.QuerySet):
    def get_related_to_user(self, user_id):
        query = Q(owner_id=user_id)
        return self.filter(query)

    def get_by_full_number(self, full_number):
        query = Q(full_account_number=full_number)
        return self.get(query)


class BankAccountManager(models.Manager):
    def get_queryset(self):
        return BankAccountQuerySet(self.model, using=self._db)

    def get_related_to_user(self, user_id):
        return self.get_queryset().get_related_to_user(user_id)

    def get_by_full_number(self, full_number):
        return self.get_queryset().get_by_full_number(full_number)


class MoneyTransferQuerySet(models.QuerySet):
    def get_related_to_user(self, user_id):
        query = Q(source_id=user_id) | Q(destination_id=user_id)
        return self.filter(query)

    def get_related_incomes(self, user_id):
        query = Q(destination_id=user_id)
        return self.filter(query)

    def get_related_expenses(self, user_id):
        query = Q(source_id=user_id)
        return self.filter(query)

    def get_all_by_full_number(self, account_number):
        query = Q(source__full_account_number=account_number) | Q(
            destination__full_account_number=account_number
        )
        return self.filter(query)


class MoneyTransferManager(models.Manager):
    def get_queryset(self):
        return MoneyTransferQuerySet(self.model, using=self._db)

    def get_related_to_user(self, user_id):
        return self.get_queryset().get_related_to_user(user_id)

    def related_incomes(self, user_id):
        return self.get_queryset().get_related_incomes(user_id)

    def related_expenses(self, user_id):
        return self.get_queryset().get_related_expenses(user_id)

    def get_all_by_full_number(self, account_number):
        return self.get_queryset().get_all_by_full_number(account_number)
