from rest_framework import viewsets

from django.contrib.auth.models import User
from api.models import BankAccount, MoneyTransfer

from api.serializers import (
    CurrentUserSerializer,
    BankAccountSerializer,
    MoneyTransferSerializer,
)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = CurrentUserSerializer


class BankAccountViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = BankAccount.active.all()
    serializer_class = BankAccountSerializer


class MoneyTransferViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MoneyTransfer.transactions_registered.all()
    serializer_class = MoneyTransferSerializer
