from django.contrib import admin
from api.models import BankAccount, MoneyTransfer

# Register your models here.
admin.site.register(BankAccount)
admin.site.register(MoneyTransfer)
